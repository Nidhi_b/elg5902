const router = require("express").Router();
const auth = require("../middleware/auth");
const AddTraining = require("../models/AddTrainingModel")
router.post("/add",auth,async(req,res)=>{
  try{

    const {No} = req.body;
    const {Exercise} = req.body;
    const {Sets} = req.body;
    const {Reps} = req.body;
    const {Tempo} = req.body;
    const {Rest} = req.body;
    const {Stretching_exercise} = req.body;
    const {SetsF} = req.body;
    const {Hold} = req.body;
    const {startDate}=req.body;
    const {endDate}=req.body
    

    //validation
    if(!startDate)
    {
     return res.status(400).json({msg:"Not all fields has been entered"});
    }
    if(!endDate)
    {
     return res.status(400).json({msg:"Not all fields has been entered"});
    }
    if(!No)
    {
     return res.status(400).json({msg:"Not all fields has been entered"});
    }
    if(!Exercise)
    {
     return res.status(400).json({msg:"Not all fields has been entered"});
    }
    if(!Sets)
    {
     return res.status(400).json({msg:"Not all fields has been entered"});
    }
    if(!Reps)
    {
     return res.status(400).json({msg:"Not all fields has been entered"});
    }
    if(!Tempo)
    {
     return res.status(400).json({msg:"Not all fields has been entered"});
    }
    if(!Rest)
    {
     return res.status(400).json({msg:"Not all fields has been entered"});
    }
    if(!Stretching_exercise)
    {
     return res.status(400).json({msg:"Not all fields has been entered"});
    }
    if(!SetsF)
    {
     return res.status(400).json({msg:"Not all fields has been entered"});
    }
    if(!Hold)
    {
     return res.status(400).json({msg:"Not all fields has been entered"});
    }




    
     const newAddTraining = new AddTraining({
         No,
         Exercise,
         Sets,
         Reps,
         Tempo,
         Rest,
         Stretching_exercise,
         SetsF,
         Hold,
         startDate,
         endDate,
         userId:req.user
     })
     const savedAddTraining = await newAddTraining.save();
     res.json(savedAddTraining);
  }catch(err){
      res.status(500).json({error:err.message});
  }
});
router.get("/all",auth,async(req,res)=>{
    const AddTrainings = await AddTraining.find({userId:req.user});
    res.json(AddTrainings);
})



module.exports=router;